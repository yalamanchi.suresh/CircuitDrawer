import { Component, HostListener, OnDestroy, OnInit } from '@angular/core';
import { Graph, Cell, Color, Markup, ToolsView, DataUri } from '@antv/x6';
import { ElkNode, ElkEdge, ElkExtendedEdge } from 'elkjs/lib/elk.bundled.js';

declare const ELK: any;
declare const bootstrap: any;

@Component({
  selector: 'app-x6-elk',
  templateUrl: './x6-elk.component.html',
  styleUrls: ['./x6-elk.component.scss']
})
export class X6ElkComponent implements OnInit, OnDestroy {

  private graphS: Graph;
  public selectedSample = "x6sample1.json";

  private layoutOptions =  {
    "portConstraints": "FREE",
    "spacing.edgeEdge": 20,
    "spacing.edgeNode": 150,
    "spacing.nodeNode": 50,
    "spacing.edgeEdgeBetweenLayers": 30,
    "spacing.edgeNodeBetweenLayers": 80,
    // "elk.layered.spacing.nodeNodeBetweenLayers": 100,
    "spacing.nodeSelfLoop": 230,
    "elk.layered.nodePlacement.bk.fixedAlignment": "BALANCED",
    "elk.layered.spacing.baseValue": 200
  }


  constructor() { }

  ngOnInit(): void {
    this.loadData();
  }

  loadData(): void {
    const elk = new ELK();
    const container = document.getElementById('circuit1');
    const graph = new Graph({
      grid: true,
      container: container!,
      interacting: true,
      panning: {
        enabled: true
      },
      mousewheel: {
        enabled: true,
        // modifiers: ['alt']
      },
      snapline: true,
      onPortRendered(args) {
        const selectors = args.contentSelectors;
        const container = selectors && selectors['portBody'] as any;
        const tooltip = new bootstrap.Tooltip(container, {title: "test sts"})
      }
    });
    const portIdToNodeIdMap: Record<string, string> = {};
    const cells: Cell[] = [];

    graph.on('node:click', ({ node }) => {
      node.attr({
        body: {
          stroke: Color.randomHex()
        }});
    });

    graph.on('edge:click', ({edge }) => {
      const edgeEl = document.querySelector("[data-cell-id='" + edge.id + "']");
      const labelEl = edgeEl.childNodes[2] as any;
      if (labelEl) {
        const tooltip = bootstrap.Tooltip.getOrCreateInstance(labelEl, {title: "test edge", placement: "auto"});
        tooltip.show();
      }
      // edge.attr({
      //   fill: {
      //     stroke: Color.randomHex()
      //   }});

      const color =  Color.randomHex();
      edge.attr({
        line1: {
          stroke: color
        },
        line2: {
          stroke: color
        }});
    });

    graph.on('edge:mouseleave', ({edge }) => {
      const edgeEl = document.querySelector("[data-cell-id='" + edge.id + "']");
      const labelEl = edgeEl.childNodes[2] as any;
      if (labelEl) {
        const tooltip = bootstrap.Tooltip.getOrCreateInstance(labelEl, {title: "test edge", placement: "auto"});
        tooltip.hide();
      }
      // edge.attr({
      //   fill: {
      //     stroke: '#a1a1a1'
      //   }});

      const color =  '#a1a1a1';
      edge.attr({
        line1: {
          stroke: color
        },
        line2: {
          stroke: color
        }});

    });

    graph.on('node:mouseleave', ({ node }) => {
      node.attr({
        body: {
          stroke: "#000"
        }});
    });



    fetch('../assets/data/' + this.selectedSample)
    .then((response) => response.json())
    .then((data) => {

      data.layoutOptions = this.layoutOptions;

      elk.layout(data).then((res) => {
        this.addChildren(graph, portIdToNodeIdMap, cells, res.children || []);
        this.addEdges(graph, portIdToNodeIdMap, cells, res.edges || []);
        graph.resetCells(cells);
        graph.zoomToFit({
          // maxScale: 1,
          padding: 5
        });
      this.graphS = graph;
    });
  });
  }

  private addChildren = (graph: Graph, portIdToNodeIdMap: Record<string, string>, cells: Cell[], children: ElkNode[], pos?: Position) => {
    children.forEach((child) => {
      const position = {
        x: (child.x || 0) + (pos ? pos.x : 0),
        y: (child.y || 0) + (pos ? pos.y : 0),
      }
      let label: string = ''
      if (typeof child.labels === 'string') {
        label = child.labels
      } else if (Array.isArray(child.labels) && child.labels[0]) {
        label = child.labels[0].text
      }
      const node = graph.createNode({
        shape: 'rect',
        id: child.id,
        position,
        label,
        size: {
          width: child.width || 0,
          height: child.height || 0,
        },
        portMarkup: [Markup.getForeignObjectMarkup()],
        ports: {
          items: (child.ports || []).map((item) => {
            portIdToNodeIdMap[item.id] = child.id
            return {
              id: item.id,
              group: 'port',
              args: {
                x: item.x,
                y: item.y,
                step: 20
              },
              size: {
                width: item.width || 7,
                height: item.height || 7,
              },
              label: {
                position: {
                  name : 'outsideOriented',
                  args: {
                    y: -8,
                  }
                }
              },
              attrs: {
                text: {
                  text: item.labels ? item.labels[0]?.text : item.id
                }
              }
            }
          }),
          groups: {
            port: {
              position: {
                name: 'absolute',
              },
              attrs: {
                portBody: {
                  magnet: 'passive',
                  fill: '#aeaeae',
                  stroke: "#000",
                  refWidth: '100%',
                  refHeight: '100%',
                  // x: -10
                },
              },
              markup: [
                {
                  tagName: 'rect',
                  selector: 'portBody',
                },
              ],
            },
          }
        },
        attrs: {
          body: {
            fill: child.layoutOptions['fillColor'] || '#eee',
            stroke: child.layoutOptions['borderColor'] || '#000',
            strokeWidth: 2,
            rx: 4,
            ry: 4,
          },
          label: {
            refX: 0,
            refY: -4,
            textAnchor: 'start',
            textVerticalAnchor: 'bottom',
            fontSize: 10,
          },
          image: {
            'xlink:href':
              'https://cdn-icons-png.flaticon.com/128/2432/2432572.png',
            width: 24,
            height: 24,
            x: 10,
            y: 10,
          }
        },
        markup: [
          {
            tagName: 'rect',
            selector: 'body',
          },
          {
            tagName: 'rect',
            selector: 'name-rect',
          },
          {
            tagName: 'text',
            selector: 'name-text',
          },
          {
            tagName: 'image',
            selector: 'avatar',
          },
        ]
      })

      cells.push(node)

      if (child.children) {
        this.addChildren(graph, portIdToNodeIdMap, cells, child.children, position)
      }

      if (child.edges) {
        this.addEdges(graph, portIdToNodeIdMap, cells, child.edges, position)
      }
    })
  }

  private addEdges = (graph: Graph, portIdToNodeIdMap: Record<string, string>, cells: Cell[], edges: ElkEdge[], pos?: Position) => {
    edges.forEach((edge: ElkExtendedEdge) => {
      const { bendPoints = [] } = edge.sections[0]

      if (pos) {
        bendPoints.map((bendPoint: Position) => {
          bendPoint.x += pos.x
          bendPoint.y += pos.y
        })
      }

      const sourcePortId = edge.sources[0]
      const targetPortId = edge.targets[0]
      const sourceNodeId = portIdToNodeIdMap[sourcePortId]
      const targetNodeId = portIdToNodeIdMap[targetPortId]

      cells.push(
        graph.createEdge({
          shape: 'edge',
          source: {
            cell: sourceNodeId,
            port: sourcePortId,
          },
          target: {
            cell: targetNodeId,
            port: targetPortId,
          },
          vertices: bendPoints,
          // router: {
          //   name: 'manhattan'
          // },
          // connector: {
          //   name: 'jumpover',
          //   args: { radius: 10 }
          // },
          connector: {
            name: 'rounded',
            args: {
              radius: 5
            },
          },
          markup: [
            {
              tagName: 'path',
              selector: 'stroke',
            }
            ,
            {
              tagName: 'path',
              selector: 'fill',
            }
            ,
            {
              tagName: 'path',
              selector: 'wrap',
              groupSelector: 'lines',
            },
            {
              tagName: 'path',
              selector: 'line1',
              groupSelector: 'lines',
            },
            {
              tagName: 'path',
              selector: 'line2',
              groupSelector: 'lines',
            }
          ],
          attrs: {
              line: {
                stroke: '#5E6CFE',
                strokeWidth: 1,
                targetMarker: {
                  name: '',
                  width: 4,
                  height: 4
                }
              },
              // fill: {
              //   connection: true,
              //   strokeWidth: 2,
              //   strokeLinecap: 'round',
              //   fill: 'none',
              //   // stroke: {
              //   //   type: 'linearGradient',
              //   //   stops: (edge.layoutOptions && edge.layoutOptions[0]['colors']) ? edge.layoutOptions[0]['colors'] : [{ offset: '0%', color: '#a1a1a1' }]
              //   // },
              //   cursor: 'pointer',
              // },
              lines: {
                connection: true,
                strokeDasharray: '10',
                strokeLinejoin: 'round',
                fill: 'none',
                cursor: 'pointer',
              },
              line1: {
                stroke: (edge.layoutOptions && edge.layoutOptions[0]['colors']) ? edge.layoutOptions[0]['colors'][0].color : '#a1a1a1',
                strokeWidth: 2,
              },
              line2: {
                stroke: (edge.layoutOptions && edge.layoutOptions[0]['colors']) ? edge.layoutOptions[0]['colors'][1].color : '#a1a1a1',
                strokeDashoffset: 10,
                strokeWidth: 2,
              }
          },
          labels: [
            {
              attrs: {
                text: {
                  text: edge.labels && edge.labels[0]?.text ? edge.labels[0]?.text : ' ',
                  fontSize: 12,
                },
              },
              position: {
                distance: 0.30,
                offset: -10,
                options: {
                  keepGradient: true
                }
              }
            }]
          }),
      )
    })
  }

  public exportToImage(): void {
    this.graphS.toSVG((dataUri: string) => {
      DataUri.downloadDataUri(DataUri.svgToDataUrl(dataUri), 'circuit.svg');
    });
  }

  public exportToPNG(): void {
    this.graphS.toPNG((dataUri: string) => {
      DataUri.downloadDataUri(dataUri, 'circuit.png')
    }, {
      padding: {
        top: 20,
        right: 30,
        bottom: 40,
        left: 50,
      },
    })
  }

  changeData(value): void {
    this.selectedSample = value;
    this.graphS.dispose();
    this.loadData();
  }

  @HostListener('window:resize', ['$event'])
  onResize(event) {
    console.log(event.target.innerWidth);
    this.changeData(this.selectedSample);
  }

  ngOnDestroy(): void {
    this.graphS.dispose();
  }

}


interface Position {
  x: number
  y: number
}
