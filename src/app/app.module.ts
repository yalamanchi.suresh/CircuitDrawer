import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { X6Component } from './x6/x6.component';
import { GojsComponent } from './gojs/gojs.component';
import { ElkComponent } from './elk/elk.component';
import { X6ElkComponent } from './x6-elk/x6-elk.component';

@NgModule({
  declarations: [
    AppComponent,
    X6Component,
    GojsComponent,
    ElkComponent,
    X6ElkComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
