import { Component, OnInit } from '@angular/core';
import ELK from 'elkjs/lib/elk.bundled.js'

// declare const ELK: any;

@Component({
  selector: 'app-elk',
  templateUrl: './elk.component.html',
  styleUrls: ['./elk.component.scss']
})
export class ElkComponent implements OnInit {

  public selectedSample = "x6sample1.json";
  public selectedGraph = 'https://rtsys.informatik.uni-kiel.de/elklive/json.html?compressedContent=N4KARAlgJmBcAEYBOB7FAXMAacBjAFhADZRICmAdnPANoiiQwJgUCMAzNuEQIYBGZIgGdqdBujIAPTMwDK8VgBYADGBABfEAF0cYAA4ok6EQjHho1MEKXK9rLmADu0dPmoB2XfjIQA5vhl4T24eAE8UAFd0AHk9dAgUChN4BgMjADohaDJLAHUAUVkAFTV1XV4BYVF6cAlpSwARAFpWew1tDRwGC2ZrFT0AJgdnKFcPLx9-QOCwXnCo2PjE5NTDdEzsvMKS9rKaxks+205dEbGEGe8-APGQ+Zi4hKTqVYysqBzmAuLSzv2exBHPSKYYuNwXCbXablMKRB5LZ4IV7rd6fRDfHaaPbdJiAmx6ACsoNG4KCkKmt1msIWj2WL3AaRRmy+21+2PMuKs+IAbMTzmTwFcKRC7nDFk8Vgy1hsPpZ8gBBH67Locw749x80mXSY3EVU+7iulIqVvZmIBVKrEqg69fEADk1lKFuoF+rFtMRKRNTNlzAtmL+OLV-QAnI69c7oaKaQjJfppai5YqA5odNH4RL6fGjABhZboJA8CAUYyWABiACV8vkHEI9DxcMXfOlGQAFNbUVgDZR-Jxg6h25Tkl2sQeB1XMNhDGGVZJmMB1QJgfLwdjT9OGz0MCgoD4AGX4giELd4uDIAFtKEuaAAJAD6e-yZaKWHgADU70Voq3X9EAKpFLIACSDT5FopTDoEXbtGm2alqY-ycmQa52OGrqRpSczurGWaMjKaLLsmEEhLO1TiFIS6OGQQjoPYpz9gg7DcpBnYEuOC4UZYNDKOB9EkqxLEIKwbGpuxAJgMhAyDGh2pQph1IZkaXpwfhSaWmJSEoSc4BnFqgmulhMaZsaKmJn6RHKohliScCMn6TMhmKVu3qqeZ6nsjaiA2USfH8rJwoGQpm5xnhZnmhZVpWcwNm8r5emCjqUZukZSnIq54Xuda4k2RqcVOol8kGh6IUJmahGZVFXkoQ6eURgVeqOcFuGlb6GUpllmlSWGtXofVgVFThJmhWV-pstoMIDcZymMnmSQFkWJbJGAlbVsRfb8UJyhDglcmMe4PYeeJbAgjOR5kbUnHMDmq6Dg4jXFVmO77oewgng2F5Xlx96Ps+r4fl+P7wP+gEgWBY2wYyc6VWAuDsIOqE9cJ+nCTBE3YVNyKzTRhbFvBiArTWva6QO21gBhCBjodnJsLFJFnQh5H1MwrZDKj3pQ0GzCDAjOkMb1u39ejqUuWFYAYmyHWWNz67rX59loylzmmWV4uWZziDc9psvxWTfUOUFD1DS1BGq5F6v6FJJ28xt-MBfrk3C8rrVi6yasThrUk+dbcs7XbCtOSVprO6bGlS1JtPa-lAv20LSvDc7o1u55Ft6Ll3s6+TguK4HPoEYnZvuynNXp1Hfsbob03G2p7XQ9z3Ul3V0f+01RtB3nEWh1zUmsKoPX+S6MfZ81bfVxLtfd3RDe2wPzcV2lov553Ht2DLxON2XyUB8Puej7s43l4Nle5vmOOLeWVaE3skcICGpOZ12B2S5OrBp1SpEMxdTOINdAzuFbm8t2Uk9MgB5ZxvTPJeEsX0HxPhfO+T835fwAWAqBcCe8IZrA5oXXAv9FA82vtPaYbMAFz29FjeauMloEzWmveAg59KUyfogNgxc3701oPsRclgADibRUy6EhudZOvhWD4Nof3Ih-CD4YzISfBaeNloXxoXzehvsXSMOhmweubCqgfw4l-MAvCiTEMEXo8SIiCRiL5hIykeFZBlQAHLRArEUG8vxYL3UPpjORlDz6rSJiou+fVKb7wklAXw1EhHZUnlYSISAzxQwkihNosF0A8CQBEvGdAuT9BSUvCSMshBxISdUJJUlWapPSZkxJQIKn5OQrWYpkSEJlL0JwUJaSMlkCyeAIE7StD1P-kUiI8TmkcNaSCDpVTuk1PxJMgZVNrJe1iSMkpLTvJqEqV0npOTbDGIWUwiSEdhmjMSTFTZuhOnVNKdzPJizoqvxOWs8ZOULm1GmTs6Wmz6msKeWM7JNkHRTO2Yk2G8M7mHLINov5ZyUJhmBdclpYLbAQuhmQHujTVn-N6TyN5C4PmJJEahDo9yvKtExac0pQINQIpmaUnBf9iUHLRV2ClzzslAiBVsxF4zNbfNJRJDgbLsW7L0PC7ldKWnc3mfUpQwrQVwxRXiq5kreWe35ZC4S8qbnh2VQS0pFimWyuOU0xJ3MaUSs+d3VQJLNWPNNTqvQXLLn6qldajVLLfkOrdWKvVILHUYttSy6F3q1V2BSS6-1PrWUktCZ4mRcFyGnwUdQ3QdYGxNnSGQcJZAHG7jIAAIW6VRSgB5QhkCQMkVRVh6yNgoM2bNET8g5uoN2NNtbM2Ntzfm1ipN011ubMAvNrUCR9o7fWrNObm0RKLegEtFAy0VuSOwMdGaJ3ANkIIAAZnuNAehW0rt2EAA';
  private elk;
  constructor() {
    this.elk = new ELK();
  }

  ngOnInit(): void {
    this.loadData();
  }

  loadData(): void {

    fetch('../assets/data/' + this.selectedSample)
    .then((response) => response.json())
    .then((data) => {
      this.elk.layout(data).then((res) => {
        document.getElementById('root').innerHTML = "<pre>" + JSON.stringify(res, null, " ") + "</pre>"
      }).catch(console.error);
    });
  }

  changeData(value, url): void {
    this.selectedSample = value;
    this.selectedGraph = url;
    this.loadData();
  }

}
