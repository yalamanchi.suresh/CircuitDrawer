import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ElkComponent } from './elk/elk.component';
import { GojsComponent } from './gojs/gojs.component';
import { X6ElkComponent } from './x6-elk/x6-elk.component';
import { X6Component } from './x6/x6.component';

const routes: Routes = [
  { path: 'x6elk', component: X6ElkComponent},
  { path: 'elk', component: ElkComponent },
  { path: 'x6', component: X6Component },
  { path: 'gojs', component: GojsComponent },
  { path: '**', redirectTo: 'x6elk' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
